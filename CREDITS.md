# Credits

### Art

* [Dungeon Ruins Tileset](https://penusbmic.itch.io/free-dungeon-ruins-tileset) - [Penusbmic](https://penusbmic.itch.io/)
* [Ambience, Footstep, Jump SFX](https://kronbits.itch.io/freesfx) - [Kronbits](https://kronbits.itch.io/)
* [Hill Background](https://edermunizz.itch.io/free-pixel-art-hill) - [edermunizz](https://edermunizz.itch.io/)
* [Mountain Dusk Background](https://ansimuz.itch.io/mountain-dusk-parallax-background) - [ansimuz](https://ansimuz.itch.io/)
* [Meow Knight](https://9e0.itch.io/cute-legends-cat-heroes) - [9e0](https://9e0.itch.io/)

### Programming

* [Making a Jump You Can Actually Use In Godot](https://www.youtube.com/watch?v=IOe1aGY6hXA)