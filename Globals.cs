using Godot;

public class Globals : Node
{
	public static float UnitSize = 64f;

	public override void _Ready()
	{
		Engine.SetTargetFps(Engine.GetIterationsPerSecond());
	}
}
