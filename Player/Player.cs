using Godot;
using System;

public class Player : KinematicBody2D
{
	[Export]
	public float MaxSpeed = 6f;            // Max speed and jump distance in tiles
	[Export]
	public float GroundAcceleration = 12f; // Ground acceleration in tiles
	[Export]
	public float AirAcceleration = 6f;     // Air acceleration in tiles
	[Export]
	public float JumpHeight = 4.5f;        // Jump height in tiles
	[Export]
	public float JumpTimeToPeak = 0.5f;    // Time to reach jump height in seconds
	[Export]
	public float JumpTimeToDescend = 0.4f; // Time to reach ground from jump peak in seconds
	[Export]
	public float JumpReleaseDecay = 48f;   // Jump power decay when released early in tiles
	[Export]
	public int   MaxAirJumps = 1;          // Maximum number of mid-air jumps
	[Export]
	public float JumpHeightDecay = 0.8f;   // Jump power decay for multiple jumps
	[Export]
	public float GroundFriction = 15.5f;   // Ground friction in tiles
	[Export]
	public float AirFriction = 3f;         // Air friction in tiles

	/**
	 * Physics Internals
	 */
	public Vector2 Velocity = Vector2.Zero;
	public float JumpVelocity = 0f;
	public float JumpGravity = 0f;
	public float FallGravity = 0f;
	public float HorizontalInput = 0f;
	public int JumpPhase = 0;
	private bool WasOnFloor = true;

	/**
	 * Audiovisual Internals
	 */
	private AnimationPlayer AP;
	private string LastAnimation = "";
	private Sprite S;
	private AudioStreamPlayer SFX;
	private AudioStreamPlayer JumpSFX;
	private OpenSimplexNoise Noise = new OpenSimplexNoise();
	
	public override void _Ready()
	{
		float h = 2f * JumpHeight * Globals.UnitSize;
		JumpVelocity = -h / JumpTimeToPeak;
		JumpGravity = h / (JumpTimeToPeak * JumpTimeToPeak);
		FallGravity = h / (JumpTimeToDescend * JumpTimeToDescend);

		AP = GetNode<AnimationPlayer>("AnimationPlayer");
		S = GetNode<Sprite>("Sprite");
		SFX = GetNode<AudioStreamPlayer>("AudioStreamPlayer");
		JumpSFX = GetNode<AudioStreamPlayer>("JumpAudioStreamPlayer");
		
		Noise.Seed = (int)GD.Randi();

		PlayAnimation("Idle");
	}

	public override void _PhysicsProcess(float delta)
	{
		HorizontalInput = Mathf.Stepify(
			Input.GetActionStrength("right") - Input.GetActionStrength("left"),
			1f
		);

		if (HorizontalInput != 0f)
		{
			if (IsOnFloor())
			{
				LandOr("Run");
			}

			S.FlipH = HorizontalInput < 0f;
			ApplyAcceleration(delta);
		}
		else
		{
			if (IsOnFloor())
			{
				LandOr("Idle");
			}
		}

		if (IsOnFloor() && !WasOnFloor)
		{
			PlayAnimation("Land");
			JumpPhase = 0;
		}

		if (Input.IsActionJustPressed("jump"))
		{
			JumpAction();
		}

		// Slow jump early if the button is released.
		if (JumpPhase > 0 && Velocity.y < Globals.UnitSize && !Input.IsActionPressed("jump"))
		{
			Velocity.y = Mathf.MoveToward(
				Velocity.y,
				Globals.UnitSize,
				JumpReleaseDecay * Globals.UnitSize * delta
			);
		}

		if (Velocity.y < 0f)
		{
			if (Velocity.y >= -80f)
			{
				PlayAnimation("JumpSlow");
			}
		}
		else if (Velocity.y > 0f)
		{
			PlayAnimation("Fall");
		}

		ApplyFriction(delta);
		ApplyGravity(delta);

		WasOnFloor = IsOnFloor();
		Velocity = MoveAndSlide(Velocity, Vector2.Up);
	}

	/**
	 * Physics Helpers
	 */
	private void ApplyAcceleration(float delta)
	{
		/* If the input and current velocity are different directions on
		 * the ground, double the acceleration so the player can turn
		 * around faster. Scaling value could be an exported variable.
		 */
		float Acceleration = IsOnFloor() ? GroundAcceleration : AirAcceleration;
		if (IsOnFloor() && Velocity.x * HorizontalInput < 0)
		{
			Acceleration *= 2f;
		}

		Velocity.x = Mathf.MoveToward(
			Velocity.x,
			MaxSpeed * Globals.UnitSize * HorizontalInput,
			Acceleration * Globals.UnitSize * delta
		);
	}

	private void JumpAction()
	{
		if (IsOnFloor() || JumpPhase <= MaxAirJumps)
		{
			Velocity.y = JumpVelocity * Mathf.Pow(JumpHeightDecay, JumpPhase);
			JumpPhase += 1;
			JumpSFX.Play();
			if (JumpPhase > 1)
			{
				PlayAnimation("LongerJump");
			}
			else
			{
				PlayAnimation("Jump");
			}
		}
	}

	private void ApplyFriction(float delta)
	{
		if (HorizontalInput == 0f)
		{
			float Friction = IsOnFloor() ? GroundFriction : AirFriction;
			Velocity.x = Mathf.MoveToward(
				Velocity.x,
				0f,
				Friction * Globals.UnitSize * delta
			);
		}
	}

	private void ApplyGravity(float delta)
	{
		float Gravity = Velocity.y < 0f ? JumpGravity : FallGravity;
		Velocity.y += Gravity * delta;
	}

	/**
	 * Animation Helpers
	 */
	private void PlayAnimation(string Animation)
	{
		if (LastAnimation != Animation)
		{
			AP.Play(Animation);
			LastAnimation = Animation;
		}
	}

	private void LandOr(string Animation)
	{
		if (AP.CurrentAnimation != "Land")
		{
			PlayAnimation(Animation);
		}
	}
	
	/**
	 * Animation Callbacks
	 */
	private void SetNewFootstepPitch()
	{
		float NewPitch = 1f + 0.2f * Noise.GetNoise1d(OS.GetTicksUsec());
		SFX.PitchScale = NewPitch;
	}

	private void Landed()
	{
		if (Velocity.x == 0f)
		{
			PlayAnimation("Idle");
		}
		else
		{
			PlayAnimation("Run");
		}
	}
}
