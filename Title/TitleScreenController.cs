using Godot;
using System;

public class TitleScreenController : Node2D
{
	private AnimationPlayer ArrowAnimationPlayer;

	public override void _Ready()
	{
		ArrowAnimationPlayer = GetNode<AnimationPlayer>("ArrowAnimationPlayer");
		ArrowAnimationPlayer.Play("Hidden");
		GetNode<AnimationPlayer>("TitleAnimationPlayer").Play("TitleBob");
	}

	private void OnContinueGameMouseEntered()
	{
		ArrowAnimationPlayer.Play("Continue");
	}

	private void OnNewGameMouseEntered()
	{
		ArrowAnimationPlayer.Play("New");
	}

	private void OnOptionsMouseEntered()
	{
		ArrowAnimationPlayer.Play("Options");
	}
	
	private void OnQuitMouseEntered()
	{
		ArrowAnimationPlayer.Play("Quit");
	}
}
