using Godot;
using Godot.Collections;
using System;

public struct Poi
{
	public Poi(Vector2 pos, float lw, float hw, float lad, float had)
	{
		Position = pos;
		LightWeight = lw;
		HeavyWeight = lw;
		LightActivationDistance = lad;
		HeavyActivationDistance = had;
	}

	public Vector2 Position { get; }
	public float LightWeight { get; }
	public float HeavyWeight { get; }
	public float LightActivationDistance { get; }
	public float HeavyActivationDistance { get; }
}

public class CameraController : Camera2D
{
	[Export]
	Vector2 CameraWeight = new Vector2(0.1f, 0.25f);
	[Export]
	bool CameraShakeDisplacementEnabled = true;
	[Export]
	bool CameraShakeRotationEnabled = true;
	[Export]
	float MaxTranslationDistance = 15f;
	[Export]
	float MaxRotationAngle = 0.15f;
	[Export]
	Vector2 UpperBounds = new Vector2(8832f, 96f);	// Right-bottom
	[Export]
	Vector2 LowerBounds = new Vector2(896f, 0f);	// Left-top

	public float Shake;

	private Player Player;
	private OpenSimplexNoise NoiseX = new OpenSimplexNoise();
	private OpenSimplexNoise NoiseY = new OpenSimplexNoise();
	private OpenSimplexNoise NoiseRot = new OpenSimplexNoise();
	private uint SeedX;
	private uint SeedY;
	private uint SeedRot;
	private Dictionary<String, Poi> Pois = new Dictionary<String, Poi>();

	public override void _Ready()
	{
		Player = GetNode<Player>("/root/World/Player");
		SeedX = GD.Randi();
		SeedY = SeedX + 1;
		SeedRot = SeedX + 2;
		NoiseX.Seed = (int)SeedX;
		NoiseY.Seed = (int)SeedY;
		NoiseRot.Seed = (int)SeedRot;
	}

	public override void _Process(float delta)
	{
		Vector2 newPosition = new Vector2(Player.Position.x, Position.y);

	// 	/**
	// 	 * Follow/move to player vertically only under certain conditions:
	// 	 *  - On floor
	// 	 *  - Falling when walking off a platform.
	// 	 *  - Falling below height of previous jump (i.e. jumping down).
	// 	 * The latter two can probably be combined since both are falling
	// 	 * below the y-coordinate at which the player left the ground.
	// 	 */
	// 	if (Player.IsOnFloor() ||
	// 		(!Player.Jumping && Player.Velocity.y > 0f) ||
	// 		(Player.Jumping && Player.Falling && Player.Position.y > Player.LastJumpY))
	// 	{
	// 		newPosition.y = Player.Position.y;
	// 	}

	// 	// Apply "pull" towards any nearby points of interest.
	// 	foreach (var poi in Pois.Values)
	// 	{
	// 		float distance = Player.Position.DistanceTo(poi.Position);
	// 		if (distance <= poi.LightActivationDistance && distance > poi.HeavyActivationDistance)
	// 		{
	// 			newPosition = newPosition.LinearInterpolate(poi.Position, poi.LightWeight);
	// 		}
	// 		else if (distance <= poi.HeavyActivationDistance)
	// 		{
	// 			newPosition = newPosition.LinearInterpolate(poi.Position, poi.HeavyWeight);
	// 		}
	// 	}

	// 	// Update shake and apply if enabled and is above 0.
	// 	UpdateShake();

	// 	if (CameraShakeDisplacementEnabled && Shake > 0f)
	// 	{
	// 		float newOffsetX = NoiseX.GetNoise1d(OS.GetTicksUsec() * delta * Engine.TimeScale / 10f);
	// 		float newOffsetY = NoiseY.GetNoise1d(OS.GetTicksUsec() * delta * Engine.TimeScale / 10f);
	// 		newPosition.x += MaxTranslationDistance * Shake * newOffsetX;
	// 		newPosition.y += MaxTranslationDistance * Shake * newOffsetY;
	// 	}

	// 	if (CameraShakeRotationEnabled && Shake > 0f)
	// 	{
	// 		float newRotation = NoiseRot.GetNoise1d(OS.GetTicksUsec() * delta * Engine.TimeScale / 10f);
	// 		Rotation = MaxRotationAngle * Shake * newRotation;
	// 	}

		if (newPosition.x < LowerBounds.x)
		{
			newPosition.x = LowerBounds.x;
		}
		else if (newPosition.x > UpperBounds.x)
		{
			newPosition.x = UpperBounds.x;
		}

		if (newPosition.y < LowerBounds.y)
		{
			newPosition.y = LowerBounds.y;
		}
		else if (newPosition.y > UpperBounds.y)
		{
			newPosition.y = UpperBounds.y;
		}

		// Final position update.
		Position = Position.LinearInterpolate(newPosition, CameraWeight);
	}

	private void UpdateShake()
	{
	}

	public void AddPointOfInterest(String name, Vector2 position, float lightWeight, float heavyWeight, float lightActivationDistance, float heavyActivationDistance)
	{
		if (Pois.ContainsKey(name))
		{
			Pois.Remove(name);
		}

		Pois.Add(name, new Poi(position, lightWeight, heavyWeight, lightActivationDistance, heavyActivationDistance));
	}

	public void RemovePointOfInterest(String name)
	{
		if (Pois.ContainsKey(name))
		{
			Pois.Remove(name);
		}
	}
}
