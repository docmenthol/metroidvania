using Godot;
using System;
using System.Collections.Generic;

public class DebugUI : Godot.CanvasLayer
{
	[Export]
	bool TraumaShakeEnabled = true;
	[Export]
	bool CameraPositionEnabled = true;
	[Export]
	bool PlayerPositionEnabled = true;
	[Export]
	bool PlayerStateEnabled = true;

	Player Player;
	CameraController Camera;

	// ColorRect TraumaBar;
	// ColorRect ShakeBar;
	Label CameraCoordsLabel;
	Label PlayerCoordsLabel;
	Label PlayerStateLabel;

	public override void _Ready()
	{
		// Scenes to debug
		Player = GetNode<Player>("/root/World/Player");
		Camera = GetNode<CameraController>("/root/World/Camera2D");

		// Debug UI elements
		// TraumaBar = GetNode<ColorRect>("TraumaBar");
		// ShakeBar = GetNode<ColorRect>("ShakeBar");
		CameraCoordsLabel = GetNode<Label>("CameraCoords");
		PlayerCoordsLabel = GetNode<Label>("PlayerCoords");
		PlayerStateLabel = GetNode<Label>("PlayerState");
	}

	public override void _Process(float delta)
	{
		// if (TraumaShakeEnabled)
		// {
		// 	TraumaBar.Show();
		// 	ShakeBar.Show();

		// 	TraumaBar.MarginTop = 590 - (515 * Player.Trauma);
		// 	ShakeBar.MarginTop = 590 - (515 * Camera.Shake);
		// }

		if (CameraPositionEnabled)
		{
			CameraCoordsLabel.Show();
			CameraCoordsLabel.Text = "Camera Pos\nX: " + Mathf.Stepify(Camera.Position.x, 0.0001f) + "\nY: " + Mathf.Stepify(Camera.Position.y, 0.0001f) + "\nR: " + Mathf.Stepify(Camera.Rotation, 0.0001f);
		}
		else
		{
			CameraCoordsLabel.Hide();
		}

		if (PlayerPositionEnabled)
		{
			PlayerCoordsLabel.Show();
			PlayerCoordsLabel.Text = "Player Pos\nX: " + Mathf.Stepify(Player.Position.x, 0.001f) + "\nY: " + Mathf.Stepify(Player.Position.y, 0.001f);
		}
		else
		{
			PlayerCoordsLabel.Hide();
		}

		if (PlayerStateEnabled)
		{
			PlayerStateLabel.Show();
			List<String> playerStates = new List<String>();

			if (Player.Velocity.x != 0f)
			{
				playerStates.Add("Moving");
			}

			// if (Player.Jumping)
			// {
			// 	playerStates.Add("Jumping");
			// }

			// if (Player.Falling)
			// {
			// 	playerStates.Add("Falling");
			// }

			if (playerStates.Count == 0)
			{
				playerStates.Add("Standing");
			}

			// PlayerStateLabel.Text =
			// 	"CanJump? " +
			// 	(Player.CanInputJump ? "Yes | " : "No  | ") +
			// 	String.Join(" && ", playerStates);
		}
		else
		{
			PlayerStateLabel.Hide();
		}
		
	}
}
